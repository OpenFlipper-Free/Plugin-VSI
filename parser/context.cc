/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/



//== INCLUDES =================================================================
#include "context.hh"
#include "input.hh"
#include "output.hh"
#include "type.hh"
#include "function.hh"

#include "types/number.hh"
#include "types/string.hh"
#include "types/bool.hh"
#include "types/filename.hh"
#include "types/selection.hh"
#include "types/vec3d.hh"
#include "types/vec4d.hh"
#include "types/matrix4x4.hh"
#include "types/objectId/objectId.hh"
#include "types/any.hh"

#include <QRegularExpression>

#define DATA_NAME "DataFlow"

//== NAMESPACES ===============================================================
namespace VSI {

//=============================================================================
//
//  CLASS VSI::Context - IMPLEMENTATION
//
//=============================================================================

/// Constructor
Context::Context (LoggingInterface *_loggingInterface, PythonInterface *_pythonInterface) :
  loggingInterface_(_loggingInterface), pythonInterface_(_pythonInterface)
{
  // add start element
  Element *e = new Element (this, "start");
  elements_.append (e);

  e->category_ = "Undefined";

  e->shortDesc_ = "Start";
  e->longDesc_ = "Start";

  e->dataOut_ = new Output (e);
  e->dataOut_->name_ = "data";
  e->dataOut_->type_ = "data";

  e->dataOut_->shortDesc_ = DATA_NAME;
  e->dataOut_->longDesc_ = DATA_NAME;

  e->flags_ = ELEMENT_FLAG_NO_DELETE | ELEMENT_FLAG_SKIP_TOOLBOX;

  // add end element
  e = new Element (this, "end");
  elements_.append (e);

  e->category_ = "Undefined";

  e->shortDesc_ = "End";
  e->longDesc_ = "End";

  e->dataIn_ = new Input (e);
  e->dataIn_->name_ = "data";
  e->dataIn_->type_ = "data";

  e->dataIn_->shortDesc_ = DATA_NAME;
  e->dataIn_->longDesc_ = DATA_NAME;

  e->flags_ = ELEMENT_FLAG_NO_DELETE | ELEMENT_FLAG_SKIP_TOOLBOX;


  // Register default types
  registerType (new TypeNumber ());
  registerType (new TypeBool ());
  registerType (new TypeString ());
  registerType (new TypeFilename ());
  registerType (new TypeSelection ());
  registerType (new TypeVec3D ());
  registerType (new TypeVec4D ());
  registerType (new TypeMatrix4x4 ());
  registerType (new TypeObjectId ());
  registerType (new TypeAny ());
}

//------------------------------------------------------------------------------

/// Destructor
Context::~Context ()
{
  foreach (Element *e, elements_)
    delete e;
  foreach (Type *t, types_)
    delete t;
}

//------------------------------------------------------------------------------

/// Returns all available elements for a given category
QVector <Element *> Context::elements (const QString& _category)
{
  QVector <Element *> rv;
  foreach (Element *e, elements_)
    if (e->category () == _category)
      rv.append (e);
  return rv;
}

//------------------------------------------------------------------------------

/// Returns the element with a given name
Element * Context::element (const QString& _name)
{
  foreach (Element *e, elements_)
    if (e->name () == _name)
      return e;
  return NULL;
}

//------------------------------------------------------------------------------

/// List of categories
QStringList Context::categories ()
{
  QStringList sl;

  foreach (Element *e, elements_)
    if (!sl.contains (e->category ()) && !(e->flags () & ELEMENT_FLAG_SKIP_TOOLBOX))
      sl << e->category ();

  return sl;
}

//------------------------------------------------------------------------------

/// Parse xml file content
void Context::parse (QFile& _xml)
{

    QDomDocument doc("OpenFlipper");
    if (!doc.setContent(&_xml)) {
        return;
    }

    // Iterate over all elements in the file (One Level below the OpenFlipper Tag
    QDomElement docElem = doc.documentElement();

    QDomNode n = docElem.firstChild();
    while(!n.isNull()) {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        if(!e.isNull()) {
            parseElement(e);
        }
        n = n.nextSibling();
    }
}

//------------------------------------------------------------------------------

/// parse element from xml
void Context::parseElement (QDomElement &_domElement)
{

    // If the element has no name, something is wrong here!
    if ( !_domElement.hasAttribute("name"))
        return;

   // Create element in the database with the new elements name
   Element *e = new Element (this, _domElement.attribute("name"));
   elements_.append (e);

   e->category_ = getXmlString (_domElement, "category", "Undefined");

   e->shortDesc_ = getXmlString (_domElement, "short", e->name ());
   e->longDesc_  = getXmlString (_domElement, "long", e->shortDesc_);

  // scene graph in/output for scenegraph elements
  if (strToBool (getXmlString (_domElement, "dataflow")))
  {
    e->dataIn_ = new Input (e);
    e->dataIn_->name_ = "data";
    e->dataIn_->type_ = "---";

    e->dataIn_->shortDesc_ = DATA_NAME;
    e->dataIn_->longDesc_ = DATA_NAME;
    e->dataIn_->setState (Input::NoRuntimeUserInput | Input::NoUserInput);

    e->dataOut_ = new Output (e);
    e->dataOut_->name_ = "data";
    e->dataOut_->type_ = "---";

    e->dataOut_->shortDesc_ = DATA_NAME;
    e->dataOut_->longDesc_ = DATA_NAME;
  }

  // Search through all children of the current element if we have inputs, outputs or functions
  for(QDomElement n = _domElement.firstChildElement(); !n.isNull(); n = n.nextSiblingElement() )
  {
      // Found an input Tag!
      if (n.tagName() == "inputs") {

          // Iterate over all inputs inside it
          for(QDomElement inputElement = n.firstChildElement(); !inputElement.isNull(); inputElement = inputElement.nextSiblingElement() )
          {
            Input* i =parseInput(inputElement,e);

            if (i) {
                e->inputs_.append (i);
            }
          }
      }

      // Found an input Tag!
      if (n.tagName() == "outputs") {

          // Iterate over all outputs inside it
          for(QDomElement outputElement = n.firstChildElement(); !outputElement.isNull(); outputElement = outputElement.nextSiblingElement() )
          {
            Output *o = parseOutput(outputElement,e);

            if (o) {
                 o->element_ = e;
                 e->outputs_.append (o);
            }
          }
      }


      // Found an input Tag!
      if (n.tagName() == "functions") {

          // Iterate over all outputs inside it
          for(QDomElement functionElement = n.firstChildElement(); !functionElement.isNull(); functionElement = functionElement.nextSiblingElement() )
          {
              Function *f = parseFunction (functionElement, e);
              if (f)
              {
                  e->functions_.append (f);
              }
          }

      }

  }


  // get code & precode segment
  e->precode_ = getXmlString (_domElement, "precode", "");
  e->code_ = getXmlString (_domElement, "code", "");

  if (e->precode_.contains(QRegularExpression("^\\s*\\t"))) {
      // precode contains tab symbol
      emit loggingInterface_->log(LOGWARN, "Precode block of "+e->name()+" contains tab identation");
  }
  if (e->code_.contains(QRegularExpression("^\\s*\\t"))) {
      // precode contains tab symbol
      emit loggingInterface_->log(LOGWARN, "Code block of "+e->name()+" contains tab identation");
  }

  // remove spaces at line begin
  e->precode_ = removeCommonTrailingSpaces(e->precode_);
  e->code_ = removeCommonTrailingSpaces(e->code_);

}

////------------------------------------------------------------------------------

//// parse element input from xml
Input* Context::parseInput(QDomElement& _domElement, Element *_e)
{
    Input *i = new Input (_e);

    // common part
    if (!parseInOutBase (_domElement, i))
    {
        delete i;
        return NULL;
    }

    // input states
    QString stateStr = _domElement.attribute("external");
    unsigned int state = 0;

    if (!stateStr.isEmpty () && !strToBool (stateStr))
        state |= Input::NoExternalInput;

    stateStr = _domElement.attribute("user");

    if (!stateStr.isEmpty () && !strToBool (stateStr))
        state |= Input::NoUserInput;

    stateStr = _domElement.attribute("runtime");

    if (!stateStr.isEmpty () && !strToBool (stateStr))
        state |= Input::NoRuntimeUserInput;

    stateStr = _domElement.attribute("optional");

    if (!stateStr.isEmpty () && strToBool (stateStr))
        state |= Input::Optional;

    i->state_ = state;

    return i;
}

////------------------------------------------------------------------------------

//// parse element output from xml
Output* Context::parseOutput (QDomElement& _domElement, Element *_e)
{
  Output *o = new Output (_e);

  // common part
  if (!parseInOutBase (_domElement, o))
  {
    delete o;
    return NULL;
  }

  return o;
}

////------------------------------------------------------------------------------

//// parse common input and output parts from xml
bool Context::parseInOutBase (QDomElement &_domElement, InOut *_io)
{
    QString type = _domElement.attribute("type","");

    if ( !_domElement.hasAttribute("name") || type.isEmpty() )
        return false;

    _io->name_ = _domElement.attribute("name");
    _io->type_ = type;

    _io->shortDesc_ = getXmlString (_domElement,"short", _io->name ());
    _io->longDesc_ = getXmlString (_domElement, "long", _io->shortDesc_);

  // get type hints for supported types
  if (typeSupported (type))
  {
    foreach (QString hint, supportedTypes_[type]->supportedHints ())
    {
      QString value = getXmlString (_domElement, hint , "");
      if (!value.isEmpty ())
        _io->hints_[hint] = value;
    }
  }

  return true;
}

////------------------------------------------------------------------------------

//// parse element function from xml
Function* Context::parseFunction (QDomElement& _domElement, Element *_e)
{
  QString name = _domElement.attribute("name");
  if (name.isEmpty ())
    return NULL;

  Function *f = new Function (_e, name);

  f->shortDesc_ = getXmlString (_domElement, "short", f->name ());
  f->longDesc_ = getXmlString (_domElement, "long", f->shortDesc_);

  // add start element
  f->start_ = new Element (_e->context (), "start_" + _e->name () + "_" + name);

  f->start_->category_ = "Undefined";

  f->start_->shortDesc_ = "Start";
  f->start_->longDesc_ = "Start";

  f->start_->dataOut_ = new Output (f->start_);
  f->start_->dataOut_->name_ = "data";
  f->start_->dataOut_->type_ = "data";

  f->start_->dataOut_->shortDesc_ = DATA_NAME;
  f->start_->dataOut_->longDesc_ = DATA_NAME;

  f->start_->flags_ = ELEMENT_FLAG_NO_DELETE | ELEMENT_FLAG_SKIP_TOOLBOX;

  elements_.append (f->start_);

  // add end element
  f->end_ = new Element (_e->context (), "end_" + _e->name () + "_" + name);

  f->end_->category_ = "Undefined";

  f->end_->shortDesc_ = "End";
  f->end_->longDesc_ = "End";

  f->end_->dataIn_ = new Input (f->end_);
  f->end_->dataIn_->name_ = "data";
  f->end_->dataIn_->type_ = "data";

  f->end_->dataIn_->shortDesc_ = DATA_NAME;
  f->end_->dataIn_->longDesc_ = DATA_NAME;

  f->end_->flags_ = ELEMENT_FLAG_NO_DELETE | ELEMENT_FLAG_SKIP_TOOLBOX;

  elements_.append (f->end_);



  // Search through all children of the current element if we have inputs, outputs or functions
  for(QDomElement n = _domElement.firstChildElement(); !n.isNull(); n = n.nextSiblingElement() )
  {
      // Found an input Tag!
      if (n.tagName() == "inputs") {

          // Iterate over all inputs inside it
          for(QDomElement inputElement = n.firstChildElement(); !inputElement.isNull(); inputElement = inputElement.nextSiblingElement() )
          {
              Output *o = new Output (f->start_);
              if (parseInOutBase(inputElement, o))
              {
                  f->start_->outputs_.append (o);
              }
              else
                  delete o;
          }

          // Only one input element allowed
          break;
      }

      // Found an input Tag!
      if (n.tagName() == "outputs") {

          // Iterate over all outputs inside it
          for(QDomElement outputElement = n.firstChildElement(); !outputElement.isNull(); outputElement = outputElement.nextSiblingElement() )
          {
              Input *i = new Input (f->end_);
              if (parseInOutBase(outputElement, i))
              {
                  i->state_ = Input::NoUserInput | Input::NoRuntimeUserInput;
                  f->end_->inputs_.append (i);
              }
              else
                  delete i;
          }

          // Only one output element allowed
          break;
      }
  }

  // add end node only if we have outputs
  if (!f->end_->inputs ().isEmpty ())
  {
    // Generate end node return code

    QString endCode = "return { ";
    foreach (Input *i, f->end_->inputs ())
      endCode += "\"" + i->name () + "\" : [input=\"" + i->name () + "\"], ";
    endCode.remove (endCode.length () - 2, 2);
    endCode += " };\n";

    f->end_->code_ = endCode;
  }

  return f;
}

//------------------------------------------------------------------------------

/// Gets the string of a tag or returns the default
QString Context::getXmlString (QDomElement &_domElement, const QString& _tag, QString _default)
{

    // Secrh through all children of the current element if it matches the given tag
    for(QDomElement n = _domElement.firstChildElement(); !n.isNull(); n = n.nextSiblingElement() )
    {
        if (n.tagName() == _tag)
            return n.text();
    }

    return _default;
}

//------------------------------------------------------------------------------

/// Converts the given string to bool
bool Context::strToBool (const QString& _str)
{
  if (_str == "1" || _str == "true"  || _str == "True" || _str == "TRUE"  ||
      _str == "yes"  || _str == "Yes"  || _str == "YES")
    return true;
  return false;
}

//------------------------------------------------------------------------------

/// Registers a supported datatype
void Context::registerType(Type * _type)
{
  types_.append (_type);

  foreach (const QString &s, _type->supportedTypes())
    supportedTypes_.insert (s, _type);
}

//------------------------------------------------------------------------------

/// Is the given type supported
bool Context::typeSupported(const QString& _type)
{
  return supportedTypes_.contains (_type);
}

//------------------------------------------------------------------------------

/// Can the given types be converted to each other
bool Context::canConvert(const QString& _type1, const QString& _type2)
{
  if (!typeSupported (_type1) || !typeSupported (_type2))
    return false;
  return supportedTypes_[_type1]->canConvertTo (_type2) ||
         supportedTypes_[_type2]->canConvertTo (_type1);
}

//------------------------------------------------------------------------------

/// Get type object for given type name
Type * Context::getType(const QString& _type)
{
  if (typeSupported (_type))
    return supportedTypes_[_type];
  return NULL;
}

//------------------------------------------------------------------------------

/// Removes trailing spaces from string which are present in each line - relative trailing spaces are not removed
//      test
//        abcd
//      qwertz
// is transformed to
// test
//  abcd
// qwertz
QString Context::removeCommonTrailingSpaces(const QString& in) {
    QStringList lines = in.split("\n");

    const int INF = 1e6;
    int commonTrailingSpaces = INF;

    for (int line=0;line<lines.length();line++) {
        int lenWithoutTrailingSpaces = QString(lines[line]).replace(QRegularExpression ("^\\s*"), "").length();

        if (lenWithoutTrailingSpaces > 0) {
            // line not empty
            int trailingSpaces = lines[line].length() - lenWithoutTrailingSpaces;

            if (trailingSpaces < commonTrailingSpaces) {
                // here are fewer trailing spaces than before discovered
                commonTrailingSpaces = trailingSpaces;
            }
        } else {
            // line empty
        }
    }

    if (commonTrailingSpaces >= INF) {
        // no content
        return QString("");
    } else {

        for (int line=0;line<lines.length();line++) {
            int subLength = lines[line].length() - commonTrailingSpaces;

            if (subLength < 0) {
                // line is empty
                lines[line] = QString("");
            } else {
                // remove common trailing whitespaces
                lines[line] = lines[line].right(subLength);
            }
        }

        QString output = lines.join("\n");
        return output;
    }
}

void Context::executeScript(const QString& _script) {
    emit pythonInterface_->executePythonScript(_script);
}

void Context::openScriptInEditor(const QString& _script) {
    emit pythonInterface_->openPythonScriptInEditor(_script);
}

}
